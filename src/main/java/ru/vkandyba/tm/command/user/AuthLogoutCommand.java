package ru.vkandyba.tm.command.user;

import ru.vkandyba.tm.command.AbstractCommand;

public class AuthLogoutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "logout";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }
}
