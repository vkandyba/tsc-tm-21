package ru.vkandyba.tm.exception.empty;

import ru.vkandyba.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty");
    }

}
