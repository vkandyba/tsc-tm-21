package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public Boolean existsByIndex(String userId, Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        return entity != null;
    }

    @Override
    public Boolean existsById(String userId, String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        return entity != null;
    }

    @Override
    public E findById(String userId, String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(String userId, Integer index) {
        final List<E> entityList = findAll(userId);
        return entityList.get(index);
    }

    @Override
    public E removeById(String userId, String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(String userId, Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E add(String userId, E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(String userId, E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll(String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(String userId, Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

}
