package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public Boolean existsByIndex(Integer index) {
        if (index < 0) return false;
        return index < list.size();
    }

    @Override
    public Boolean existsById(String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        return entity != null;
    }

    @Override
    public E findById(String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(Integer index) {
        return list.get(index);
    }

    @Override
    public E removeById(String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E add(E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        return list.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

}
