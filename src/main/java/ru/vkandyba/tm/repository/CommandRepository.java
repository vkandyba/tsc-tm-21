package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.constant.ArgumentConst;
import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.model.Command;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getListCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getListCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }
}
